#!/bin/bash

BACKUPS_PATH=~/backup
DOCKER_PROJECTS_PATH=~/projects/vps/docker
DOCKER_DATA_PATH=/opt/services

BACKUP_DATE=$(date -d "today" +"%Y-%m-%d_%H-%M-%S")
CURRENT_BACKUP_PATH=${BACKUPS_PATH}/${BACKUP_DATE}
mkdir -p ${CURRENT_BACKUP_PATH}

cd ${DOCKER_PROJECTS_PATH}
TO_BACKUP=( "emb.heldt.pl" "embeddedlinux.pl" "kubasejdak.com" )
for SITE in "${TO_BACKUP[@]}"; do
    echo "Creating backup of \"${SITE}\"":

    echo "    * Stopping Docker containers..."
    cd ${DOCKER_PROJECTS_PATH}/${SITE}
    docker-compose down

    echo "    * Creating archive from volumes..."
    cd ${DOCKER_DATA_PATH}
    sudo tar -zcvf ${SITE}.tar.gz ${SITE}
    sudo chown kuba:kuba ${SITE}.tar.gz
    mv ${SITE}.tar.gz ${CURRENT_BACKUP_PATH}/

    echo "    * Starting Docker containers..."
    cd ${DOCKER_PROJECTS_PATH}/${SITE}
    . ${DOCKER_DATA_PATH}/${SITE}/set-environment.sh
    docker-compose up -d

    echo "    * Done"
done

echo "Removing obsolete backup files..."
TO_REMOVE=( `ls -t ${BACKUPS_PATH} | awk 'NR>3'` )

for BACKUP in "${TO_REMOVE[@]}"; do
    REMOVE_PATH=${BACKUPS_PATH}/${BACKUP}
    echo "    * Removing ${REMOVE_PATH}..."
    rm -rf ${REMOVE_PATH}
done

echo "Backup complete"
